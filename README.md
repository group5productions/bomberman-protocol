# Protokollspezifikation für Dynamite Boy und kompatible Actionspiele

Protokoll finalisiert am: 20. März 2017 um 01:39.

Dieses Dokument beschreibt ein Protokoll für ein netzwerkbasiertes Actionspiel, dessen Gameplay an Bomberman angelehnt ist. Das Spiel umfasst drei Bestandteile: einen Spieleserver, der das Spiel hostet, einen Verwaltungsserver, über den Spieleserver gefunden werden können, sowie einen Client, über den die Spieler an Runden teilnehmen können.

## Inhaltsverzeichnis

* [Grundlagen des Protokolls](protocol.md)
* [Verwaltungsserver](management-server.md)
* [Spieleserver](game-server.md)
